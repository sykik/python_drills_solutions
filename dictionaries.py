from collections import Counter
import string
def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    puncs = string.punctuation
    puncs = puncs.replace('-','')  #because we have words like general-purpose in our test case so if not in your test case
    for i in s:
        if i in puncs:
            s = s.replace(i,"")
    s = s.split()
    return dict(Counter(s))


def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    return [(i,j) for i,j in d.items()]


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    return [(i,j) for i,j in sorted(d.items())]
