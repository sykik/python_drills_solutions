"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    with open(path,'r',encoding = 'utf-8') as f:
        s = f.read()
    return s


def write_to_file(path, s):
    with open(path,'w',encoding = 'utf-8') as f:
        f.write(s)


def append_to_file(path, s):
    with open(path,'a',encoding = 'utf-8') as f:
        f.write(s)


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    with open(file_path,'w',encoding = 'utf-8') as f:
        for i in range(1,n+1):
            f.write(f'{i},{i**2}\n')