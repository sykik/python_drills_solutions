import json
def html_dict_search(html_dict, selector):
    """
    Implement `id` and `class` selectors
    """
    result = []
    def select(data,key):
        if key in data['attrs']:
            if selector == data['attrs'][key]:
                result.append(data)
        for data in data['children']:
            select(data,key)
    if selector.startswith('.'):
        key = 'class'
    elif selector.startswith('#'):
        key = 'id'
    selector = selector[1:]
    for data in html_dict['children']:
        select(data,key)
    return result