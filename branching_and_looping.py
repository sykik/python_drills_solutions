def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    return [i for i in range(start,end,step)]


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    int_list = []
    if step > 0:
        while(start != end and start < end):
            int_list.append(start)
            start += step
    else:
        while(start != end and start > end):
            int_list.append(start)
            start += step
    return int_list


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    pr_list = []
    for i in range(10,100):
        for n in range(2, i):
            if (i % n) == 0:
                break
        else:
            pr_list.append(i)
    return pr_list